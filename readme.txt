assessment :
1. Create documentation for test cases on http://automationpractice.com
(backup if not available http://www.practiceselenium.com)
-> Please check file testcase.xlsx

2. Find at least 1 bugs on the application, and create appropriate bug report.
-> Please check file bug_report.docx

3. Create at least 1 test automation script
(UI test / performance test / api tests) based on the test cases or bug report
-> Please check folder UI automation for UI test
-> please check loadtest.py for loadtest script (loadtest using locustio)

notes:
-for loadtest using locustio, there are two endpoint, 1st is working endpoint with response 200,
2nd is not working endpoint with response 403 which is bug
