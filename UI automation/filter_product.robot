*** Settings ***
Test Setup         Open Browser    ${SERVER}     ${BROWSER}
Test Teardown      Close Browser
Resource            resource.robot

*** Test Case ***
Test Case check filter categories on women page
    Location Should Be                  ${SERVER}
    Title Should be                     My Store
    Click Element                       sizzle=.sf-with-ul:nth(0)
    Wait Until Element contains         sizzle=.title_block:nth(0)    WOMEN
    Click Element                       sizzle=.col-lg-6:contains("Tops") input
    Wait Until Element Is Not Visible   sizzle=.product_list p:nth(0)     5
Test Case check filter size on women page
    Location Should Be                  ${SERVER}
    Title Should be                     My Store
    Click Element                       sizzle=.sf-with-ul:nth(0)
    Wait Until Element contains         sizzle=.title_block:nth(0)    WOMEN
    Click Element                       sizzle=.col-lg-6:contains("S") input
    Wait Until Element Is Not Visible   sizzle=.product_list p:nth(0)     5
