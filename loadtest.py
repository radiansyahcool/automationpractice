from locust import HttpLocust, TaskSet, task
import resource
resource.setrlimit(resource.RLIMIT_NOFILE, (10240, 9223372036854775807)) #delete this line if not run in MAC

domain = 'http://automationpractice.com'
class Practice_automate(TaskSet):
    @task(1)
    def women_menu(self): #load test for working women menu, will result success request
        endpoint = '/index.php?id_category=3&controller=category'
        response = self.client.request(method="GET" ,
                                        url= domain + endpoint ,
                                        headers={'cache-control':"no-cache",} ,
                                       )

    @task(1)
    def filter_tops(self): #load test for not working filter categories (tops selected), will result fail request
        endpoint = '/modules/blocklayered/blocklayered-ajax.php?layered_category_4=4&id_category_layered=3&layered_price_slider=16_53&orderby=position&orderway=asctrue&_=1559395274273'
        response = self.client.request(method="GET" ,
                                        url= domain + endpoint,
                                        headers={'cache-control':"no-cache",} ,
                                       )

class LoadTestApi(HttpLocust):
     task_set = Practice_automate
     min_wait = 5000
     max_wait = 15000
